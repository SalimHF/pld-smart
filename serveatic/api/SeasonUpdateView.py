from django.http import HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from .models import Ingredient
from .webcrawler.SeasonCrawler import updateSeason
import json
class SeasonUpdateView(View):
    @csrf_exempt
    def put(self, request):
        """
        {
            "recipe_url": "anUrl"
        }
        """
        newData = updateSeason()
        ingredients = list(Ingredient.objects.all())
        for i in range (0,len(ingredients)):
            ingredients[i].seasonal = newData[i]["seasonal"]
            ingredients[i].save()
        return HttpResponse()
