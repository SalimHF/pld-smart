from django.views import View
from .model.Searcher import Searcher
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json

class SearchView (View):
    @csrf_exempt
    def post(self, request):
        """
        {
            "ingredients":
            [
                {
                    "name":"tomate"
                },
                {
                    "name":"basilic"
                }
            ]
        }
        """
        #print(request.body)
        return HttpResponse(json.dumps(Searcher.search(json.loads(request.body)),ensure_ascii=False))
        """ 
        {
            "recipes":
            [
                {
                    "difficulty": 1,
                    "img_link": "https://assets.afcdn.com/recipe/20200227/108329_w648h414c1cx1916cy2488cxt0cyt1095cxb3840cyb4025.jpg",
                    "ingredients": [
                        {
                            "ingredient": {
                                "name": "pâte brisée"
                            },
                            "quantity": {
                                "nbr": 1,
                                "unit": ""
                            }
                        },
                        {
                            "ingredient": {
                                "name": "crème épaisse"
                            },
                            "quantity": {
                                "nbr": 20,
                                "unit": "cl"
                            }
                        },
                        {
                            "ingredient": {
                                "name": "concentré de tomates"
                            },
                            "quantity": {
                                "nbr": 1,
                                "unit": "petite boîte"
                            }
                        },
                        {
                            "ingredient": {
                                "name": "thon"
                            },
                            "quantity": {
                                "nbr": 150,
                                "unit": "g"
                            }
                        },
                        {
                            "ingredient": {
                                "name": "sel"
                            },
                            "quantity": {}
                        },
                        {
                            "ingredient": {
                                "name": "herbes de Provence"
                            },
                            "quantity": {}
                        },
                        {
                            "ingredient": {
                                "name": "fromage râpé"
                            },
                            "quantity": {
                                "nbr": 50,
                                "unit": ""
                            }
                        },
                        {
                            "ingredient": {
                                "name": "oeuf"
                            },
                            "quantity": {
                                "nbr": 1,
                                "unit": ""
                            }
                        }
                    ],
                    "link": "https://www.marmiton.org/recettes/recette_tarte-thon-et-tomate_14139.aspx",
                    "name": "Tarte thon et tomate",
                    "price": 1,
                    "recipeType": "entree",
                    "servings": 6,
                    "time": {
                        "cooking": 25,
                        "preparation": 10,
                        "rest": 0
                    }
                }
            ]
        }
        """