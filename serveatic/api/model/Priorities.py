import copy
from datetime import date, timedelta
from api.models import Ingredient, Quantity
import math
class PriorityLevel():
    def __init__(self, priorities, level=0):
        super().__init__()
        self.level=level
        self.priorities=[Priority.specific(priority) for priority in priorities]

    def determine(self, request, recipe):
        if not hasattr(recipe, "priorities"):
            recipe.priorities=[]
        [priority.determine(request, recipe) for priority in self.priorities]
        recipe.priorities.append(copy.deepcopy(self))
        return recipe

    def toDict(self):
        return {self.level:[priority.toDict() for priority in self.priorities]}

    def __str__(self):
        return str(self.toDict())

class Priority():
    @staticmethod
    def specific(priority):
        assert "name" in priority.keys(), "No priority name"
        assert priority["name"] in Priority.labelsToSpecific.keys(), "Unrecognized priority: "+ priority["name"]

        return Priority.labelsToSpecific[priority["name"]].specific(priority)
    
    def __init__(self, priority):
        super().__init__()

    def determine(self, request, recipe):
        return self

    def toDict(self):
        return {}

    def __str__(self):
        return str(self.toDict())

class OverallPriority(Priority):
    key="overall"

    @staticmethod
    def specific(priority):
        return OverallPriority(priority)
    
    def __init__(self, priority):
        super().__init__(priority)

    def determine(self, request, recipe):
        overall=0
        weights=0
        for priorityLevel in recipe.priorities:
            weight = 1/(1+int(priorityLevel.level))
            for priority in priorityLevel.priorities:
                overall += weight*priority.value
                weights+=weight

        if weights > 0:
            overall /= weights
        self.value =overall
        return self

    def toDict(self):
        priorityDict=super().toDict()
        priorityDict={"name":OverallPriority.key}
        if hasattr(self, "value"):
            priorityDict["value"]=self.value
        return priorityDict

    def __str__(self):
        return str(self.toDict())

class ExpirationDatePriority(Priority):
    key="expDate"

    @staticmethod
    def specific(priority):
        return ExpirationDatePriority(priority)
    
    def __init__(self, priority):
        super().__init__(priority)

    def determine(self, request, recipe):
        todayDate=date.today()
        leastDelta=timedelta.max
        numberOfIngredientOfLeastDelta=0
        if "ingredients" in request.keys():
            for ingredient in request["ingredients"]:
                if ingredient["name"] in [ingredient.name for ingredient in recipe.ingredients]:
                    if "expDate" in ingredient.keys():
                        (year,month,day)=ingredient["expDate"].split("/")
                        expDate=date(int(year),int(month),int(day))
                        currentDelta=(expDate-todayDate)

                        if currentDelta<leastDelta:
                            leastDelta=currentDelta
                            numberOfIngredientOfLeastDelta=0

                        if currentDelta==leastDelta:
                            numberOfIngredientOfLeastDelta+=1

        priorityValue=(1/(1+leastDelta.days))*math.log(1+numberOfIngredientOfLeastDelta,2)
        self.value=priorityValue
        return self

    def toDict(self):
        priorityDict=super().toDict()
        priorityDict={"name":ExpirationDatePriority.key}
        if hasattr(self, "value"):
            priorityDict["value"]=self.value
        return priorityDict

    def __str__(self):
        return str(self.toDict())

class SeasonalityPriority(Priority):
    key="seasonal"

    @staticmethod
    def specific(priority):
        return SeasonalityPriority(priority)
    
    def __init__(self, priority):
        super().__init__(priority)

    def determine(self, request, recipe):
        seasonalPriorityValue=0
        seasonalIngredientsCount=0
        for ingredient in recipe.ingredients:
            if hasattr(ingredient, "seasonal") and ingredient.seasonal is not None:
                seasonalIngredientsCount+=1
                if ingredient.seasonal:
                    seasonalPriorityValue+=1
        if seasonalIngredientsCount>0:
            seasonalPriorityValue/=seasonalIngredientsCount

        self.value=seasonalPriorityValue
        return self

    def toDict(self):
        priorityDict=super().toDict()
        priorityDict["name"]=SeasonalityPriority.key
        if hasattr(self, "value"):
            priorityDict["value"]=self.value
        return priorityDict

    def __str__(self):
        return str(self.toDict())

class IngredientPresencePriority(Priority):
    key="ingredientPresence"

    @staticmethod
    def specific(priority):
        return IngredientPresencePriority(priority)
    
    def __init__(self, priority):
        super().__init__(priority)

    def determine(self, request, recipe):

        ratioTotal = 0
        ingredients = []

        servings = request.get("servings", 1)

        for recipeIngredient in recipe.ingredients:
            userIngredient=None
            if "ingredients" in request.keys():
                for currentUserIngredient in request["ingredients"]:
                    if currentUserIngredient["name"]==recipeIngredient.name:
                        userIngredient=currentUserIngredient
                        break

            ratioIngredient = 1

            if hasattr(recipeIngredient, "quantity") and recipeIngredient.quantity.nbr>0:
                ratioIngredient, ingredientManquant = 0, copy.deepcopy(recipeIngredient)
                if userIngredient is not None and "quantity" in userIngredient.keys() and userIngredient["quantity"]["unit"]==recipeIngredient.quantity.unit:                     
                    (ratioIngredient,quantiteManquante) = Quantity.fromDict(userIngredient["quantity"]).isQuantityEnough(recipeIngredient.quantity, servings/recipe.servings)
                    ingredientManquant.quantity = quantiteManquante

                if ratioIngredient < 1:
                    ingredients.append(ingredientManquant)
                else: 
                    ratioIngredient=1

            elif userIngredient is None:# and recipeIngredient has None or 0 quantity
                ratioIngredient=0
                ingredients.append(copy.deepcopy(recipeIngredient))

            ratioTotal += ratioIngredient

        ratioTotal /= len(recipe.ingredients) # in foreach so len>=1

        self.value = ratioTotal
        self.ingredients = ingredients
        return self

    def toDict(self):
        priorityDict=super().toDict()
        priorityDict={"name":IngredientPresencePriority.key, "value": self.value, "ingredients":[eval(str(i)) for i in self.ingredients]}
        return priorityDict

    def __str__(self):
        return str(self.toDict())


Priority.labelsToSpecific = {
    OverallPriority.key:OverallPriority,
    ExpirationDatePriority.key : ExpirationDatePriority, 
    SeasonalityPriority.key : SeasonalityPriority, 
    IngredientPresencePriority.key : IngredientPresencePriority
}
