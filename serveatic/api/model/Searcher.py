from api.models import Recipe, Ingredient
import json
from .Criteria import *
from .Priorities import *
class Searcher(object):

    @staticmethod
    def absoluteFilters(request, querySet):
        if "criteria" in request.keys():
            nonDBCriteriaList=[]
            for criterion in request["criteria"]:
                criterion=Criterion.specific(criterion)
                if type(criterion) in Criterion.DBFilters:
                    querySet=criterion.filter(querySet)
                else:
                    nonDBCriteriaList.append(criterion)
            recipeList=list(querySet)
            for criterion in nonDBCriteriaList:
                recipeList=criterion.filter(recipeList)
        return recipeList

    @staticmethod
    def ingredientsFilter(request, recipeList):

        recipesToKeep = []

        servings = request.get("servings", 1)

        if "ingredients" in request.keys():
            userIngredients = Ingredient.sumQuantitiesByIngredient([ Ingredient.fromDict(ingredient) for ingredient in request["ingredients"]])
            userIngredientNames=[ ingredient.name for ingredient in userIngredients]

            for recipe in recipeList:
                aRecipe = recipe
                keepRecipe = True

                for recipeIngredient in aRecipe.ingredients:
                    if recipeIngredient.name not in userIngredientNames:
                        keepRecipe = False
                        break
                    else :
                        userMatchingIngredient = [ingredient for ingredient in userIngredients if ingredient.name == recipeIngredient.name]
                        assert(len(userMatchingIngredient)==1)
                        userMatchingIngredient = userMatchingIngredient[0]

                        if recipeIngredient.quantity is not None and recipeIngredient.quantity.nbr > 0:
                            if userMatchingIngredient.quantity is None:
                                keepRecipe=False
                                break

                            if not userMatchingIngredient.quantity.isQuantityEnough(recipeIngredient.quantity, ratio=servings/aRecipe.servings):
                                keepRecipe=False
                                break
                if keepRecipe:
                    recipesToKeep.append(aRecipe)

        recipeList = recipesToKeep        

        return recipeList

    @staticmethod
    def priorityDetermination(request, recipeList):
        if "priorities" in request.keys():
            for level, priorities in request["priorities"].items():
                priorityLevel = PriorityLevel(priorities, level)
                recipeList=[priorityLevel.determine(request, recipe) for recipe in recipeList]
        return recipeList

    @staticmethod
    def priorityScaling(request, recipeList):
        for recipe in recipeList:
            if hasattr(recipe,"priorities"):
                Searcher.scalePriorities(recipe.priorities)
        return recipeList

        return recipeList

    @staticmethod
    def scalePriorities(priorities):

        return priorities

    @staticmethod
    def weightedAverage(priorities):
        overall=0
        weights=0
        for priorityLevel in priorities:
            weight = 1/(1+int(priorityLevel.level))
            for priority in priorityLevel.priorities:
                overall += weight*priority.value
                weights+=weight

        if weights > 0:
            overall /= weights
        
        return overall

    @staticmethod
    def prioritySort(recipe):
        return [priorityLevel.priorities[0].value for priorityLevel in recipe.priorities if priorityLevel.level=="overall"][0]

    @staticmethod
    def recipeRanking(request, recipeList):
        """
        Electre III
        """
        recipeList = [ PriorityLevel([{"name":"overall"}], "overall").determine(request,recipe) for recipe in recipeList]
        # Sort
        #print(recipeList)
        recipeList.sort(key=Searcher.prioritySort, reverse=True)
        #print(recipeList)
        return recipeList

    @staticmethod
    def recipeSelection(request, recipeList, amount=15):
        return recipeList[:15]

    @staticmethod
    def recipeFormatting(request, recipeList):
        for recipe in recipeList:
            for ingredient in recipe.ingredients:
                if hasattr(ingredient, "quantity"):
                    ingredient.quantity.nbr*=request["servings"]/recipe.servings
            recipe.servings=request["servings"]
        return recipeList

    @staticmethod
    def search(request):
        '''
        {
            "ingredients": [
                {"name": "tomates" },
                {"name": "cerises" }
            ],
            "criteria": [
                {"class":"quant", "name":"difficulty", "lB":"1":"uB":"+Inf"},
                {"class":"qual", "name":"recipeType", "value":"dessert"}
            ],
            "priorities":[
                {0:[{"name":"difficulty"},{"name":"recipeType"}]},
                {1:[{}]}
                {2:[{}]}
            ]
        }
        '''
        print("Retrieving Recipes")
        querySet = Recipe.objects.all()
        print("Absolute Filters")
        recipeList=Searcher.absoluteFilters(request,querySet)
        if "onlyOwned" in request.keys() and request["onlyOwned"]:
            recipeList=Searcher.ingredientsFilter(request,recipeList)
        print("Priority Determination")
        recipeList=Searcher.priorityDetermination(request,recipeList)
        recipeList=Searcher.priorityScaling(request, recipeList)
        recipeList=Searcher.recipeRanking(request, recipeList)
        recipeList=Searcher.recipeSelection(request, recipeList)
        recipeList=Searcher.recipeFormatting(request, recipeList)
        print("Serializing")
        recipes={"recipes":[eval(str(r)) for r in recipeList]}
        print("Sending Response")
        return recipes
