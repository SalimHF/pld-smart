from api.models import Recipe, Ingredient

class Criterion():
    @staticmethod
    def specific(criterion):
        assert criterion["class"], "No criterion class in:"+str(criterion)
        assert criterion["class"] in Criterion.labelsToSpecific.keys(), "Unrecognized criterion class:"+str(criterion["class"])

        return Criterion.labelsToSpecific[criterion["class"]].specific(criterion)
    
    def __init__(self, criterion):
        super().__init__()

    def filter(self, recipeList):
        return recipeList

class QuantitativeCriterion(Criterion):
    key="quant"
    @staticmethod
    def specific(criterion):
        assert criterion["name"], "No criterion name in:"+str(criterion)
        assert criterion["name"] in QuantitativeCriterion.labelsToSpecific.keys(), "Unrecognized criterion name:"+str(criterion["name"])
        assert criterion["lB"], "No criterion lower bound in:"+str(criterion)
        assert criterion["uB"], "No criterion upper bound in:"+str(criterion)
        
        return QuantitativeCriterion.labelsToSpecific[criterion["name"]].specific(criterion)

    def __init__(self, criterion):
        super().__init__(criterion)
        self.lB=int(criterion["lB"])
        self.uB=int(criterion["uB"])

    def filter(self, recipeList):
        return recipeList

class DifficultyCriterion(QuantitativeCriterion):
    key="difficulty"
    def __init__(self, criterion):
        super().__init__(criterion)

    @staticmethod
    def specific(criterion):
        return DifficultyCriterion(criterion)

    def filter(self, querySet):
        return querySet.filter(difficulty__gte=self.lB,difficulty__lte=self.uB)

class PrepTimeCriterion(QuantitativeCriterion):
    key="prepTime"
    def __init__(self, criterion):
        super().__init__(criterion)

    @staticmethod
    def specific(criterion):
        return PrepTimeCriterion(criterion)

    def filter(self,recipeList):

        recipesToKeep = []

        for recipe in recipeList:
            aRecipe = recipe

            temps = aRecipe.time

            if temps is not None:
                prepTime=getattr("temps", "cooking", 0)+getattr("temps", "preparation", 0)+getattr("temps", "rest", 0)

                if self.lB <= prepTime and prepTime <= self.uB:
                    
                    recipesToKeep.append(aRecipe)


        recipeList = recipesToKeep        

        return recipeList

class QualitativeCriterion(Criterion):
    key="qual"
    def __init__(self, criterion):
        super().__init__(criterion)
        self.value = criterion["value"]

    @staticmethod
    def specific(criterion):
        assert criterion["name"], "No criterion name in:"+str(criterion)
        assert criterion["name"] in QualitativeCriterion.labelsToSpecific.keys(), "Unrecognized criterion name:"+str(criterion["name"])
        assert criterion["value"], "No criterion value in:"+str(criterion)

        return QualitativeCriterion.labelsToSpecific[criterion["name"]].specific(criterion)

    def filter(self, recipeList):
        return recipeList

class RecipeTypeCriterion(QualitativeCriterion):
    key="recipeType"
    def __init__(self, criterion):
        super().__init__(criterion)

    def filter(self, querySet):
        return querySet.filter(recipeType=self.value)

    @staticmethod
    def specific(criterion):
        return RecipeTypeCriterion(criterion)

Criterion.DBFilters = [DifficultyCriterion, RecipeTypeCriterion]
Criterion.labelsToSpecific = {QuantitativeCriterion.key:QuantitativeCriterion, QualitativeCriterion.key:QualitativeCriterion}
QualitativeCriterion.labelsToSpecific = {RecipeTypeCriterion.key:RecipeTypeCriterion}
QuantitativeCriterion.labelsToSpecific = {DifficultyCriterion.key:DifficultyCriterion,PrepTimeCriterion.key:PrepTimeCriterion}