from django.http import HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from .webcrawler.MarmitonCrawler import MarmitonCrawler
import json
class RecipeStepsView(View):
    @csrf_exempt
    def post(self, request):
        """
        {
            "recipe_url": "anUrl"
        }
        """
        recipe_url = json.loads(request.body)["recipe_url"]
        return HttpResponse(json.dumps(MarmitonCrawler.getRecipeSteps(recipe_url),ensure_ascii=False))
