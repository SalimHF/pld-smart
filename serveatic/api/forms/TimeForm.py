from django import forms
from api.models import Time

class TimeForm (forms.ModelForm):
    class Meta:
        model = Time
        fields = ['cooking','preparation','rest']