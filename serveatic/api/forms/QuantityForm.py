from django import forms
from api.models import Quantity
class QuantityForm(forms.ModelForm):

    class Meta:
        model = Quantity
        fields = ['nbr','unit']
