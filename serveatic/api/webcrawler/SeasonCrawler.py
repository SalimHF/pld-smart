import locale

# french locale
locale.setlocale(locale.LC_ALL, 'fr_FR')
from datetime import datetime
from bs4 import BeautifulSoup
import urllib.request
import requests
from .constant import *


# Update the seasonal attribute of ingredients thanks to green peace website
def updateSeason():
    seasonalIngredients = []

    # green peace crawling
    html = urllib.request.urlopen(SEASON_URL).read()

    soup = BeautifulSoup(html, 'html.parser')

    # retrieve all seasonal ingredients over the year
    articleIter = soup.findAll('article', {"class": "list-items"})
    for article in articleIter:
        ingredientsIter = article.findAll("li")
        for ingredient in ingredientsIter:
            ingredientName = formatString(ingredient.get_text())
            if ingredientName not in seasonalIngredients:
                seasonalIngredients.append(ingredientName)

    # retrieve all ingrediendts from database

    url = API_BASE_URL + "/ingredient/"
    response = requests.get(url)
    data = response.json()

    currentMonth = datetime.now().strftime('%B')

    #current seasonal ingredients

    seasonalLegumes = []
    seasonalFruits = []
    seasonalCereal = []


    legumesIter = soup.find("a", {"id": currentMonth + "-legumes"}).next_sibling()[4].findAll("li")

    for legume in legumesIter:
        legumeName = formatString(legume.get_text())
        seasonalLegumes.append(legumeName)

    fruitsIter = soup.find("a", {"id": currentMonth + "-fruits"}).next_sibling()[4].findAll("li")
    for fruit in fruitsIter:
        fruitName = formatString(fruit.get_text())
        seasonalFruits.append(fruitName)

    cerealSoup = soup.find("a", {"id": currentMonth + "-cereales"})
    if cerealSoup is not None:
        cerealIter = cerealSoup.next_sibling()[4].findAll("li")
        for cereal in cerealIter:
            cerealName = formatString(cereal.get_text())
            seasonalCereal.append(cerealName)

    for ingredient in data:

        # format data to be similar to data found in green peace website
        ingredientNameFormat = ingredient["name"].replace("-", " ")

        # reset ingredient season to false if the ingredient can be a seasonal ingredient or None if it can't be
        if ingredientNameFormat in seasonalIngredients:
            ingredient["seasonal"] = False
        else:
            ingredient["seasonal"] = None

        # if the ingredient name find in database is a substring of a seasonal ingredient we update the boolean to true
        for legume in seasonalLegumes:
            if legume.replace("-", " ").find(ingredientNameFormat) != -1:
                ingredient["seasonal"] = True
        for fruit in seasonalFruits:
            if fruit.replace("-", " ").find(ingredientNameFormat) != -1:
                ingredient["seasonal"] = True
        for cereal in seasonalCereal:
            if cereal.replace("-", " ").find(ingredientNameFormat) != -1:
                ingredient["seasonal"] = True

    return data


def formatString(str):
    return str.strip(' \t\n\r').lower().replace("é", "e").replace("è", "e")
