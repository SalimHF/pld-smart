import requests

from serveatic.api.webcrawler.MarmitonCrawler import MarmitonCrawler, API_BASE_URL

print("<------ Retrieving marmiton ingredients ------>")
MarmitonCrawler.getMarmitonIngredient()



print("<------ Updating season ingredients ------>")

url = API_BASE_URL + "/seasonUpdate/"
response = requests.put(url)


print("<------ Retrieving marmiton recipes ------>")

MarmitonCrawler.getMarmitonRecipes()

