from bs4 import BeautifulSoup
import urllib.parse
import urllib.request
import requests
import json

from .constant import *


class MarmitonCrawler:

    @staticmethod
    def getMarmitonRecipes():

        RECIPE_TYPES = ["entree", "platprincipal", "dessert", "amusegueule", "accompagnement", "sauce", "boisson",
                        "confiserie"]
        # we are requesting marmiton using a recipe type criteria
        for type in RECIPE_TYPES:
            recipesData = []

            # starting page 1 of results with the first recipes
            pageNumber = 1
            recipeNumber = 1
            queryParams = {
                "aqt": "",
                "pht": 1,  # we only considered recipe with photo
                "st": 1,
                "page": pageNumber,
                "start": recipeNumber,
                "dt": type
            }

            paramsUrl = urllib.parse.urlencode(queryParams)

            fullUrl = SEARCH_BASE_URL + paramsUrl

            html = urllib.request.urlopen(fullUrl).read()

            soup = BeautifulSoup(html, 'html.parser')

            print(fullUrl)

            # first page to retrieve the number of pages and first recipes
            pageNumbers = soup.find("div", {"class": "showMorePages"}).findAll('li')[
                -1].get_text()  # retrieve number of pages that contain results
            recipesData += (MarmitonCrawler.constructRecipeResults(html))
            pageNumber += 1
            recipeNumber += 15
            # then when iterate over the next pages with other recipes
            for i in range(2, int(pageNumbers) + 1):
                queryParams = {
                    "aqt": "",
                    "pht": 1,  # we only considered recipe with photo
                    "st": 1,
                    "page": pageNumber,
                    "start": recipeNumber,
                    "dt": type
                }
                paramsUrl = urllib.parse.urlencode(queryParams)

                fullUrl = SEARCH_BASE_URL + paramsUrl
                html = urllib.request.urlopen(fullUrl).read()
                pageNumber += 1
                recipeNumber += 15
                print(fullUrl)
                recipesData += MarmitonCrawler.constructRecipeResults(html)

            #post recipes
            url = API_BASE_URL + "/recipe/"
            requests.post(url, json=recipesData)

    @staticmethod
    def constructRecipeResults(htmlContent):

        recipesData = []

        soup = BeautifulSoup(htmlContent, 'html.parser')
        recipeCards = soup.findAll("a", {"class": "recipe-card-link"})

        iterrecipes = iter(recipeCards)
        for recipeLink in iterrecipes:
            fullUrl = MARMITON_BASE_URL + recipeLink["href"]
            try:
                recipe = MarmitonCrawler.getRecipeDetails(fullUrl)
                if recipe not in recipesData:
                    recipesData.append(recipe)
            except:
                pass
        return recipesData

    # Retrieve all recipe details from the recipe url
    @staticmethod
    def getRecipeDetails(url):

        recipeHTML = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(recipeHTML, 'html.parser')

        scriptIter = soup.findAll("script")
        scriptContentInfo = scriptIter[20].contents[0].strip(' \t\n\r')  # script location on html file
        scriptRecipesData = scriptIter[10].contents[0].strip(' \t\n\r')  # script location on html file
        contentInfo = scriptContentInfo[
                      len("m_dataLayer = [{ \contentInfo\": "):scriptContentInfo.find('}') + 1].replace('\'',
                                                                                                        '"')  # we isolate script data and format it to match with json format
        recipesData = scriptRecipesData[len("var Mrtn = Mrtn || {}; Mrtn.recipesData = "):-1]

        # json format
        jsonContentInfo = json.loads(contentInfo)
        jsonRecipesData = json.loads(recipesData)

        name = jsonContentInfo["recipeTitle"]

        img = soup.find("div", {"class": "af-pin-it-wrapper diapo diapoV"}).find("img")["src"]
        type = jsonContentInfo["recipeType"]

        servings = jsonContentInfo["recipeServings"]

        difficulty = difficultyToInt[jsonContentInfo["recipeDifficulty"]]

        preparation = soup.find("div", {"class": "recipe-infos__timmings__preparation"}).get_text().strip(' \t\n\r')

        # a recipe may not have rest time or cooking time
        try:
            rest = soup.find("div", {"class": "recipe-infos__timmings__rest"}).get_text().strip(' \t\n\r')
        except:
            rest = ""
        try:
            cooking = soup.find("div", {"class": "recipe-infos__timmings__cooking"}).get_text().strip(' \t\n\r')
        except:
            cooking = ""

        ingredients = []
        ingredientsData = jsonRecipesData["recipes"][0]["ingredients"]
        ingredientsNames = jsonContentInfo["recipeIngredients"].split(',')
        for i in range(len(ingredientsData)):
            ingredientData = {
                "quantity": {}
            }
            ingredientData["name"] = ingredientsNames[i]
            nbr = ingredientsData[i]["qty"]
            if nbr != '':
                ingredientData["quantity"]["nbr"] = int(nbr)
            unit = ingredientsData[i]["unit"]
            if nbr != '':
                ingredientData["quantity"]["unit"] = unit

            ingredients.append(ingredientData)
        # final structure of a recipe
        return {
            "name": name,
            "link": url,
            "img_link": img,
            "recipeType": type,
            "difficulty": difficulty,
            "servings": servings,
            "time": {
                "preparation": MarmitonCrawler.timeStringToInt(preparation),
                "rest": MarmitonCrawler.timeStringToInt(rest),
                "cooking": MarmitonCrawler.timeStringToInt(cooking)
            },
            "ingredients": ingredients,
        }

    # Retrieve all ingredients from Marmiton and store them in the local

    @staticmethod
    def getRecipeSteps(url):

        recipeHTML = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(recipeHTML, 'html.parser')
        steps = []
        stepsIter = soup.findAll("li", {"class": "recipe-preparation__list__item"})
        for step in stepsIter:
            steps.append(step.get_text().strip(' \t\n\r'))
        return {"steps": steps}

    @staticmethod
    def getMarmitonIngredient():
        baseUrl = MARMITON_BASE_URL + INGREDIENT_URL

        html = urllib.request.urlopen(baseUrl).read()
        soup = BeautifulSoup(html, 'html.parser')

        ingredientsData = []

        iterLetters = soup.find("ul", {"class": "letters-list"}).findAll("a")
        # iter over all starting letter
        for letter in iterLetters:
            currentLetter = letter.get_text()
            currentPageNumber = 1
            currentUrl = baseUrl + '/' + currentLetter + '/' + str(currentPageNumber)

            ingredientsByLetterHTML = urllib.request.urlopen(currentUrl).read()
            ingredientsByLetterSoup = BeautifulSoup(ingredientsByLetterHTML, 'html.parser')

            morePages = ingredientsByLetterSoup.find("div", {"class": "showMorePages"})
            # more than 9 pages of results
            if morePages is not None:
                pageNumbers = morePages.findAll('li')[-1].get_text()
            # between 1 and 9 pages of results
            else:
                pageNav = ingredientsByLetterSoup.find("nav", {"class": "af-pagination"})
                if pageNav is not None:
                    pageNumbers = pageNav.find("ul").findAll("li")[-1].get_text()

                # only one page of results
                else:
                    pageNumbers = 1

            for pageNumber in range(1, int(pageNumbers) + 1):
                currentPageNumber = pageNumber
                currentUrl = baseUrl + '/' + currentLetter + '/' + str(currentPageNumber)

                print(currentUrl)

                ingredientsByPageHTML = urllib.request.urlopen(currentUrl).read()
                ingredientsByPageSoup = BeautifulSoup(ingredientsByPageHTML, 'html.parser')

                iterIngredients = ingredientsByPageSoup.findAll('div', {"class": "index-item-card"})
                for ingredients in iterIngredients:
                    ingredientName = ingredients.find("a")["href"][len(INGREDIENT_URL) + 1:].strip(
                        ' \t\n\r').lower()
                    ingredientName = urllib.parse.unquote(ingredientName)
                    ingredientData = {
                        "name": ingredientName,
                    }
                    if ingredientData not in ingredientsData:
                        ingredientsData.append(ingredientData)
        url = API_BASE_URL + "/ingredient/"
        requests.post(url, json=ingredientsData)

    @staticmethod
    def timeStringToInt(time):
        if time == "":
            return 0
        else:
            time = time[time.find(':') + 1:].strip()  # isolation of the string "x min" or "x h y" from the InnerHTML
            if time.find("min") != -1:
                return int(time[0:time.find("min")])
            if time.find('h') != -1:
                return int(time[0:time.find('h')]) * 60 + int(time[time.find('h') + 1:])
