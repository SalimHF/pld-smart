MARMITON_BASE_URL = "https://www.marmiton.org"
SEARCH_BASE_URL = "http://www.marmiton.org/recettes/recherche.aspx?"
INGREDIENT_URL= "/recettes/index/ingredient"
API_BASE_URL = "http://localhost:8000/api"
# marmiton recipe difficulty to int
difficultyToInt = {
    "tresfacile": 1,
    "facile": 2,
    "moyenne": 3,
    "difficile": 4
}

# marmiton recipe price to int
priceToInt = {
    "bonmarche": 1,
    "moyen": 2,
    "assezcher": 3
}

SEASON_URL = "https://www.greenpeace.fr/guetteur/calendrier/"