# Generated by Django 2.2.12 on 2020-05-05 13:34

import api.forms.IngredientForm
import api.forms.QuantityForm
import api.forms.TimeForm
import api.models
from django.db import migrations, models
import djongo.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('name', models.CharField(default='ingredient', max_length=200, primary_key=True, serialize=False)),
                ('seasonal', models.NullBooleanField(default=False)),
                ('quantity', djongo.models.fields.EmbeddedModelField(model_container=api.models.Quantity, model_form_class=api.forms.QuantityForm.QuantityForm, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Quantity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nbr', models.FloatField(default=0)),
                ('unit', models.CharField(default='unit', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('name', models.CharField(default='name', max_length=200)),
                ('link', models.URLField(default='https://www.marmiton.org/recettes/recette_carottes-vichy_17717.aspx', primary_key=True, serialize=False)),
                ('img_link', models.URLField(default='https://assets.afcdn.com/recipe/20180208/77439_w648h414c1cx1200cy1800cxt0cyt0cxb2400cyb3600.jpg')),
                ('recipeType', models.CharField(default='recipeType', max_length=200)),
                ('servings', models.IntegerField(default=0)),
                ('difficulty', models.IntegerField(default=0)),
                ('ingredients', djongo.models.fields.ArrayModelField(default=list, model_container=api.models.Ingredient, model_form_class=api.forms.IngredientForm.IngredientForm)),
                ('time', djongo.models.fields.EmbeddedModelField(model_container=api.models.Time, model_form_class=api.forms.TimeForm.TimeForm, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Time',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cooking', models.IntegerField(default=0)),
                ('preparation', models.IntegerField(default=0)),
                ('rest', models.IntegerField(default=0)),
            ],
        ),
    ]
