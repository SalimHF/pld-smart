from .models import Ingredient, Recipe, Quantity, Time
from rest_framework import serializers


class QuantitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Quantity
        fields = ('nbr','unit',)

    def create(self, validated_data):

        wQuantity = Quantity.objects.create(**validated_data)

        return wQuantity

class TimeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Time
        fields = ('cooking','preparation','rest',)

    def create(self, validated_data):

        wTime = Time.objects.create(**validated_data)

        return wTime

class IngredientSerializer(serializers.HyperlinkedModelSerializer):
    #quantity=QuantitySerializer(Quantity)
    class Meta:
        model = Ingredient
        fields = ('name','seasonal', 'quantity',)
        depth=1

    def __init__(self, *args, **kwargs):
        super(IngredientSerializer, self).__init__(*args, **kwargs)
        self.fields['quantity'].required = False 
        self.fields['name'].required = True 
        self.fields['seasonal'].required = False 

    def create(self, validated_data):
        
        wIngredient = Ingredient.objects.create(**validated_data)

        return wIngredient

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        if instance.quantity is not None:
            quantity = QuantitySerializer().to_representation(instance.quantity)
            ret["quantity"]=quantity
        else:
            del ret["quantity"]

        if instance.seasonal is None:
            del ret["seasonal"]

        return ret

class RecipeSerializer(serializers.HyperlinkedModelSerializer):
    #time=TimeSerializer(Time)
    #ingredients=IngredientSerializer(Ingredient, many=True)
    class Meta:
        model = Recipe
        fields = ('name', 'link', 'img_link', 'recipeType', 'servings', 'difficulty', 'ingredients', 'time',)
        depth=2

    def create(self, validated_data):

        wRecipe = Recipe.objects.create(**validated_data)

        return wRecipe

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.link = validated_data.get('link', instance.link)
        instance.img_link = validated_data.get('img_link', instance.img_link)
        instance.recipeType = validated_data.get('recipeType', instance.recipeType)
        instance.servings = validated_data.get('servings', instance.servings)
        instance.difficulty = validated_data.get('difficulty', instance.difficulty)
        instance.ingredients = validated_data.get('ingredients', instance.ingredients)
        instance.time = validated_data.get('email', instance.time)
        instance.save()
        return instance

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ingredients = [IngredientSerializer().to_representation(ingredient) for ingredient in instance.ingredients]
        ret["ingredients"]=ingredients
        time = TimeSerializer().to_representation(instance.time)
        ret["time"]=time
        return ret
