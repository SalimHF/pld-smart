import os
import sys
import requests
# If you are using a Jupyter notebook, uncomment the following line.
# %matplotlib inline
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from PIL import Image
from io import BytesIO
import json

#Environment Variables
os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY'] = '8a294e52cd75419db1e1575c6c369d2d'
os.environ['COMPUTER_VISION_ENDPOINT'] = 'https://francecentral.api.cognitive.microsoft.com/'

class newIngredient:
    labels = []
    realLabel = ''
    quantity = 0.0

    def __init__(self,labels,quantity,realLabel):
        self.labels = labels
        self.quantity = quantity
        self.realLabel = realLabel

    def __str__(self):
        return "Real Label = "+ self.realLabel + "| Labels = " + ', '.join(str(label) for label in self.labels) + "| Quantity = " + str(self.quantity)



# Au niveau du front, suite à la prise en photo du ticket de caisse + validation :
# - Ouverture d'une pop-up tab-bar avec deux sections :
#     - Première section : 'Ingrédients reconnus' avec affichage du label+unité+quantité
#                           + Un bouton modifier + Un bouton valider
#     - Deuxième section : 'Ingrédients non reconnus' (qui n'ont pas matché le dico d'ingrédients)
#                           avec affichage du label+unité+qt
#                           + Un bouton 'Ajouter l'ingrédient à la main' + Un bouton 'Réessayer' + Un bouton 'Valider'


# Limitations OCR Microsoft Vision :
#   - Le ticket de caisse pris en photo doit être uniformément illuminé (pas de zones d'ombre)
#   - 'G' pour 'grammes' (e.g '160G') est interprété comme un '6' (donc '160G' devient '1606')

# Limitations du ticket de caisse en lui-même :
#   - Ne contient pas les accents
class OCR(object):
    @staticmethod
    def runOCR(image_data,jsonDBIngredientList):
        jsonAPIResponse = OCR._processImage(image_data)
        ocrIngredientList = OCR._buildOCRIngredientList(jsonAPIResponse)
        dbIngredientList = OCR._buildDBIngredientList(jsonDBIngredientList)
        filteredIngredientList = OCR._ocrIngredientListFilter(ocrIngredientList,dbIngredientList)
        jsonResponseToClient = OCR._constructJsonIngredientList(filteredIngredientList)
        print("jsonResponseToClient = ",jsonResponseToClient)
        return jsonResponseToClient

    #Uses the Microsoft Vision API to recognize words from an input image
    #Returns the json response from the API
    @staticmethod
    def _processImage(image_data):
        # Add your Computer Vision subscription key and endpoint to your environment variables.
        if 'COMPUTER_VISION_SUBSCRIPTION_KEY' in os.environ:
            subscription_key = os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY']
        else:
            print("\nSet the COMPUTER_VISION_SUBSCRIPTION_KEY environment variable.\n**Restart your shell or IDE for changes to take effect.**")
            sys.exit()

        if 'COMPUTER_VISION_ENDPOINT' in os.environ:
            endpoint = os.environ['COMPUTER_VISION_ENDPOINT']
        else:
            print("\nSet the COMPUTER_VISION_ENDPOINT environment variable.\n**Restart your shell or IDE for changes to take effect.**")
            sys.exit()

        ocr_url = endpoint + "vision/v2.0/ocr"

        # Build HTTP POST Request
        params = {'language': 'unk', 'detectOrientation': 'true'}
        # Set Content-Type to octet-stream
        headers = {'Ocp-Apim-Subscription-Key': subscription_key, 'Content-Type': 'application/octet-stream'}
        # put the byte array into your post request
        response = requests.post(ocr_url, headers=headers, params=params, data=image_data)
        response.raise_for_status()

        analysis = response.json()
       # print("Microsoft API Response =  \n",analysis)
        return analysis

    @staticmethod
    def _buildDBIngredientList(jsonDBIngredientList):
        ingredientList = []
        ingredients = jsonDBIngredientList['ingredients']
        for ingredient in ingredients:
            ingredientList.append(ingredient['name'])
        return ingredientList

    @staticmethod
    def _buildOCRIngredientList(jsonOCRIngredientList):
        ingredientList = []
        ocrTextRegion = jsonOCRIngredientList['regions'][0]['lines']
        for line in ocrTextRegion:
            firstWordWithDigits = True
            ingredientLabels = []
            ingredientQuantity = 0.0
            for word in line['words']:
                #If the world contains a digit, we assign it to the ingredient's quantity attribute
                if firstWordWithDigits and any(ch.isdigit() for ch in word['text']):
                    ingredientQuantity = word['text'].lower()
                    firstWordWithDigits = False
                #Otherwise we add the word to the ingredient's labels list
                else:
                    ingredientLabels.append(word['text'].lower())
            ingredientList.append(newIngredient(ingredientLabels,ingredientQuantity,''))
        return ingredientList

    # Preprocessing techniques :
    #   - ...
    def ocrIngredientListPreprocessor(ocrIngredientList):
        print("cc")

    # Match the OCR ingredients with the DB list of ingredients
    # The goal is to only keep in the OCR list the ingredients that also appear in the DB list of ingredients
    # Matching technique :
    #   - v0 : OCR ingredient is to be kept if there exists a DB ingredient which is a subset of the OCR ingredient
    #   - v1 : same idea with a loosened existence condition : the DB ingredient must have (N-1) characters that match the
    #          OCR character exactly (with corresponding order obviously), N being the length of the DB ingredient
    # To ensure that the matching is done with the right DB ingredient, the OCR ingredient will be matched
    # with the DB ingredient that has the most equal characters AND the minimum overall nb of characters
    # e.g : "POIVR" in the OCR list should be matched with "poivre" and not "poivre au citron" (both of which
    #        exist in the DB ingredient list)
    #
    # Warning : the method assumes the preprocessing of the OCR list has been done beforehand
    # Possible upgrade : if the performance time isn't satisfying, looping through the dbIngredients could be made dichotomic instead of linear
    @staticmethod
    def _ocrIngredientListFilter(ocrIngredientList,dbIngredientList):
        filteredIngredientList = []
        for ocrIngredient in ocrIngredientList:
            for dbIngredient in dbIngredientList:
                '''TODO : Ne pas se limiter à la région 0'''
                #OCR ingredient is kept only if there exists a DB ingredient which is a subset of the OCR ingredient
                if dbIngredient in ocrIngredient.labels[0]:
                    filteredIngredientList.append(newIngredient([],ocrIngredient.quantity,dbIngredient))
        return filteredIngredientList

    @staticmethod
    def _constructJsonIngredientList(filteredIngredientList):
        jsonIngredientList = []
        for ingredient in filteredIngredientList:
            jsonIngredient = {}
            jsonIngredient["name"] = ingredient.realLabel
            jsonIngredient["quantity"] = ingredient.quantity
            jsonIngredientList.append(jsonIngredient)
        return {"ingredients":jsonIngredientList}



    #Displays the image and overlays it with the extracted text
    def plotOCR(jsonResponse,initialImage):
        # Extract the word bounding boxes and text.
        line_infos = [region["lines"] for region in jsonResponse["regions"]]
        word_infos = []
        for line in line_infos:
            for word_metadata in line:
                for word_info in word_metadata["words"]:
                    word_infos.append(word_info)

        # Display the image and overlay it with the extracted text.
        plt.figure(figsize=(5, 5))
        image = Image.open(BytesIO(initialImage))
        ax = plt.imshow(image, alpha=0.5)
        for word in word_infos:
            bbox = [int(num) for num in word["boundingBox"].split(",")]
            text = word["text"]
            origin = (bbox[0], bbox[1])
            patch = Rectangle(origin, bbox[2], bbox[3],
                              fill=False, linewidth=2, color='y')
            ax.axes.add_patch(patch)
            plt.text(origin[0], origin[1], text, fontsize=20, weight="bold", va="top")
        plt.show()
        plt.axis("off")


'''
# Image from local file
image_path = "Ticket_de_caisse.JPG"
image_data = open(image_path, "rb").read()
jsonResponse = OCR.processImage(image_path)
OCR.plotOCR(jsonResponse,image_data)
#Store the api's response in a json file
with open('ocr_ticket_de_caisse.json','w') as outfile:
	json.dump(jsonResponse, outfile)
'''

'''
with open('ingredients_list.json','r') as ingredientsListFile:
	jsonIngredientList = json.load(ingredientsListFile)

ingredientList = OCR.buildDBIngredientList(jsonIngredientList)

with open('ocr_ticket_de_caisse.json','r') as jsonOCRFile:
	jsonOCR = json.load(jsonOCRFile)

ocrIngredientList = OCR.buildOCRIngredientList(jsonOCR)
for ingredient in ocrIngredientList:
    print(ingredient)
print('\n\n')
filteredOcrIngredientList = OCR.ocrIngredientListFilter(ocrIngredientList,ingredientList)
for ingredient in filteredOcrIngredientList:
    print(ingredient)

'''

# Read the image into a byte array
'''
image_path = "Ticket_de_caisse.JPG"
image_data = open(image_path, "rb").read()
with open('ingredients_list.json','r') as ingredientsListFile:
	jsonIngredientList = json.load(ingredientsListFile)
OCR.runOCR(image_data,jsonIngredientList)

'''
