﻿#!/bin/sh

###
# README 
###
# This script allows you to switch between a generic version of settings.py (which is the only version that should be ever seen in a commit) to a custom version containing the server IP address.
# 
# To use this file for your own workflow:
# 0. Take this file somewhere out of the git repository. 
#    DO NOT MODIFY THE FILE IN .scripts DIRECTLY.
#
# In that new file which you pasted anywhere on your computer except in the git repository:
# 1. Replace PATH_TO_SCRIPTS with the absolute path to the files in the .scripts folder
# 2. Replace CUSTOM_IPS with a string containing the IP address you need (the server's IP address, for example: CUSTOM_IPS="10.22.21.63").
# 3. 
#    If you start working from a pulled version, launch:
#        sh <wrapper-name> custom
#    to change the values in settings.py to those in CUSTOM_IPS
#
#    If you are about to commit to the repository, launch:
#        sh <wrapper-name> generic
#    to empty the values in settings.py
#
#    If {no|an invalid} argument is given to the script, it will default to emptying settings.py
#
###

PATH_TO_SCRIPTS="/home/vester/Documents/SMART/pld-smart/.scripts"
CUSTOM_IPS="10.22.21.63"

GENERIC_TO_CUSTOM="custom"
CUSTOM_TO_GENERIC="generic"

if [ "$GENERIC_TO_CUSTOM" = "$1" ]; then
  echo "GENERIC_TO_CUSTOM"
  cd $PATH_TO_SCRIPTS && sh allowed_hosts_replacer.sh $CUSTOM_IPS
else
  echo "CUSTOM_TO_GENERIC"
  cd $PATH_TO_SCRIPTS && sh allowed_hosts_replacer.sh "0.0.0.0"
fi
cd ..
grep "ALLOWED_HOSTS" ./serveatic/serveatic/settings.py
