#!/bin/sh

#DO NOT TOUCH
sed -i "s/ALLOWED_HOSTS = \[\('[0-9]\{1,3\}\(\.[0-9]\{1,3\}\)\{3\}'\)\?\]/ALLOWED_HOSTS = \['$1'\]/" ../serveatic/serveatic/settings.py

sed -i "s/String url = 'http:\/\/[0-9]\{1,3\}\(\.[0-9]\{1,3\}\)\{3\}:8000\/api\/search';/String url = 'http:\/\/$1:8000\/api\/search';/" ../eatic/lib/view/recipes.dart
