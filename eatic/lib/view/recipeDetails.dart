import 'dart:convert';
import 'package:eatic/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:expandable/expandable.dart';
import '../controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

bool stepsLoaded = false;

class RecipeDetails extends StatefulWidget {
  var recette;
  var jsonSteps;

  RecipeDetails({Key key, this.recette}) : super(key: key);

  @override
  _RecipeState createState() => _RecipeState();
}

class _RecipeState extends State<RecipeDetails> {
  @override
  void initState() {
    super.initState();
    stepsLoaded = false;
    loadRecipeDetails();
  }

  Future<void> loadRecipeDetails() async {
    Controller controleur = new Controller();
    String url = "http://10.0.2.2:8000/api/recipeSteps/";
    var requestJson = '{"recipe_url": \"${widget.recette["link"]}\"}';
    widget.jsonSteps = await controleur.sendPostRequest(url, requestJson);
    setState(() {
      stepsLoaded = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListView(children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Card(
              borderOnForeground: true,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0),
                    ),
                    child: Image.network(widget.recette["img_link"],
                        height: 150, fit: BoxFit.cover),
                  ),
                  ListTile(
                    title: Column(
                      children: <Widget>[
                        Text(widget.recette["name"],
                            style: TextStyle(
                                fontSize: 20.0, color: new Color(0xFFF25321))),
                        Text(widget.recette["recipeType"],
                            style: TextStyle(
                                fontSize: 12.0, color: new Color(0xFFF25321))),
                      ],
                    ),
                    //TODO : Gérer les cas où la recette contient moins de 3 ingrédients
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Row(children: <Widget>[
                            new Spacer(),
                            Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    (widget.recette["difficulty"] >= 1)
                                        ? Image.asset(
                                            'assets/details_chef_full.png')
                                        : Image.asset(
                                            'assets/details_chef_empty.png'),
                                    (widget.recette["difficulty"] >= 2)
                                        ? Image.asset(
                                            'assets/details_chef_full.png')
                                        : Image.asset(
                                            'assets/details_chef_empty.png'),
                                    (widget.recette["difficulty"] >= 3)
                                        ? Image.asset(
                                            'assets/details_chef_full.png')
                                        : Image.asset(
                                            'assets/details_chef_empty.png'),
                                    (widget.recette["difficulty"] >= 4)
                                        ? Image.asset(
                                            'assets/details_chef_full.png')
                                        : Image.asset(
                                            'assets/details_chef_empty.png'),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    (widget.recette["difficulty"] == 1)
                                        ? Text("Très Facile",
                                            style: TextStyle(
                                                color: new Color(0xFFF25321)))
                                        : (widget.recette["difficulty"] == 2)
                                            ? Text("Facile",
                                                style: TextStyle(
                                                    color:
                                                        new Color(0xFFF25321)))
                                            : (widget.recette["difficulty"] ==
                                                    3)
                                                ? Text("Moyen",
                                                    style: TextStyle(
                                                        color: new Color(
                                                            0xFFF25321)))
                                                : (widget.recette[
                                                            "difficulty"] ==
                                                        4)
                                                    ? Text("Difficile",
                                                        style: TextStyle(
                                                            color: new Color(
                                                                0xFFF25321)))
                                                    : Text("Not found",
                                                        style: TextStyle(
                                                            color: new Color(
                                                                0xFFF25321))),
                                  ],
                                )
                              ],
                            ),
                            new Spacer(),
                            Column(
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Icon(
                                    Icons.people,
                                    color: new Color(0xFFF25321),
                                    size: 24.0,
                                  ),
                                ]),
                                Row(
                                  children: <Widget>[
                                    Text(
                                        (widget.recette["servings"])
                                                .toString() +
                                            " pers.",
                                        style: TextStyle(
                                            color: new Color(0xFFF25321))),
                                  ],
                                ),
                              ],
                            ),
                            new Spacer(),
                            Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.timer,
                                      color: new Color(0xFFF25321),
                                      size: 24.0,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                        (widget.recette["time"]["cooking"] +
                                                    widget.recette["time"]
                                                        ["preparation"] +
                                                    widget.recette["time"]
                                                        ["rest"])
                                                .toString() +
                                            " min",
                                        style: TextStyle(
                                            color: new Color(0xFFF25321))),
                                  ],
                                )
                              ],
                            ),
                            new Spacer(),
                            // padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
                            // )
                          ]),
                        ]),
                  ),
                ],
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              margin: EdgeInsets.all(10),
            ),
            Card(
              borderOnForeground: true,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              margin: EdgeInsets.all(10),
              child: ExpandableTheme(
                  data: ExpandableThemeData(
                      iconColor: new Color(0xFFF25321),
                      animationDuration: const Duration(milliseconds: 100)),
                  child: ExpandablePanel(
                      header: Padding(
                        padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                        child: Text("Ingrédients",
                            style: TextStyle(
                                fontSize: 20.0, color: new Color(0xFFF25321))),
                      ),
                      expanded: Padding(
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                        child: Column(
                          children: <Widget>[
                            _buildIngredients(),
                          ],
                        ),
                      ))),
            ),
            Card(
              borderOnForeground: true,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              margin: EdgeInsets.all(10),
              child: ExpandableTheme(
                  data: ExpandableThemeData(
                      iconColor: new Color(0xFFF25321),
                      animationDuration: const Duration(milliseconds: 100)),
                  child: ExpandablePanel(
                      header: Padding(
                        padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                        child: Text("Préparation",
                            style: TextStyle(
                                fontSize: 20.0, color: new Color(0xFFF25321))),
                      ),
                      expanded: Padding(
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  if (widget.recette["time"]["preparation"] > 0)
                                    Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text("Préparation",
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  color: new Color(0xFFF25321),
                                                )),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                                (widget.recette["time"]
                                                            ["preparation"])
                                                        .toString() +
                                                    " min",
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  color: new Color(0xFFF25321),
                                                )),
                                            Icon(
                                              Icons.timer,
                                              color: new Color(0xFFF25321),
                                              size: 16.0,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  if (widget.recette["time"]["cooking"] > 0)
                                    Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text("Cuisson",
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  color: new Color(0xFFF25321),
                                                )),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                                (widget.recette["time"]
                                                            ["cooking"])
                                                        .toString() +
                                                    " min",
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  color: new Color(0xFFF25321),
                                                )),
                                            Icon(
                                              Icons.timer,
                                              color: new Color(0xFFF25321),
                                              size: 16.0,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  if (widget.recette["time"]["rest"] > 0)
                                    Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text("Repos",
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  color: new Color(0xFFF25321),
                                                )),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                                (widget.recette["time"]["rest"])
                                                        .toString() +
                                                    " min",
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  color: new Color(0xFFF25321),
                                                )),
                                            Icon(
                                              Icons.timer,
                                              color: new Color(0xFFF25321),
                                              size: 16.0,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                ],
                              ),
                            ),
                            (stepsLoaded) ? _buildSteps() : Text(""),
                          ],
                        ),
                      ))),
            ),
          ],
        )
      ]),
    );
  }

  Widget _buildIngredients() {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.all(8),
        itemCount: widget.recette["ingredients"].length,
        itemBuilder: (BuildContext context, int index) {
          return IntrinsicHeight(
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        (index + 1 < 10)
                            ? (index + 1).toString() + "   - "
                            : (index + 1).toString() + " - ",
                        style: TextStyle(
                          fontSize: 14.0,
                          color: new Color(0xFFF25321),
                        )),
                  ],
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                          (widget.recette["ingredients"][index]["quantity"]
                                      ["nbr"] ==
                                  0)
                              ? widget.recette["ingredients"][index]["name"]
                              : widget.recette["ingredients"][index]["quantity"]
                                          ["nbr"]
                                      .toString() +
                                  " " +
                                  widget.recette["ingredients"][index]
                                      ["quantity"]["unit"] +
                                  " " +
                                  widget.recette["ingredients"][index]["name"],
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Color.fromRGBO(8, 60, 102, 1),
                          )),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildSteps() {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.all(8),
        itemCount: widget.jsonSteps["steps"].length,
        itemBuilder: (BuildContext context, int index) {
          return IntrinsicHeight(
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        (index + 1 < 10)
                            ? (index + 1).toString() + "   - "
                            : (index + 1).toString() + " - ",
                        style: TextStyle(
                          fontSize: 14.0,
                          color: new Color(0xFFF25321),
                        )),
                  ],
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget.jsonSteps["steps"][index],
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Color.fromRGBO(8, 60, 102, 1),
                          )),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }
}
