import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../controller/controller.dart';
import '../model/ingredient.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:convert';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';

String inputIngredientLabel; //string input from the manual "add" button
String inputIngredientQuantity; //string input from the manual "add" button
String inputIngredientUnit; //string input from the manual "add" button
String
    inputIngredientExpirationDate; //string input from the manual "add" button
bool inputIngredientSeasonality;

String toBeModifiedIngredientLabel;

List<Ingredient> ingredients = [];

var validIngredientsList = [];

bool validInputIngredient = true;

enum TextFieldType {
  ingredientLabel,
  ingredientQuantity,
  ingredientUnit,
  ingredientExpirationDate
}

List<String> getValidIngredientsNames() {
  List<String> validIngredientsNames = [];
  for (int i = 0; i < validIngredientsList.length; i++) {
    validIngredientsNames.add(validIngredientsList[i].getName());
  }
  return validIngredientsNames;
}

class FoodStock extends StatefulWidget {
  @override
  _FoodStockState createState() => _FoodStockState();
}

class _FoodStockState extends State<FoodStock> {
  //TODO : vérifier si la double instanciation du contrôleur ne crée pas de bloquage
  Controller controller = Controller();

  void initiateValidIngredientsList() async {
    validIngredientsList = await controller.getIngredientsFromDb();
  }

  bool verifyIngredientsExistence() {
    for (int i = 0; i < validIngredientsList.length; i++) {
      if (inputIngredientLabel == validIngredientsList[i].getName()) {
        inputIngredientSeasonality = validIngredientsList[i].isSeasonal();
        return true;
      }
    }
    return false;
    //return validIngredientsList.contains(inputIngredientLabel);
  }

  void deleteIngredient(Ingredient ingredient) async {
    Future<dynamic> deletedIngredient =
        await controller.deleteIngredient(ingredient);
    List<Ingredient> temp = await updateViewIngredients();
    setState(() {});
  }

  void modifyIngredient() async {
    //Verify that the new input label is one of a valid ingredient
    if (verifyIngredientsExistence()) {
      validInputIngredient = true;
      //modify the ingredient in the Client DB
      Future<dynamic> modifiedIngredient = await controller.updateIngredient(
          toBeModifiedIngredientLabel,
          new Ingredient(
              name: inputIngredientLabel,
              quantity: double.parse(inputIngredientQuantity),
              unit: inputIngredientUnit,
              expirationDate: inputIngredientExpirationDate,
              seasonal: inputIngredientSeasonality));

      List<Ingredient> temp = await updateViewIngredients();
      setState(() {});
    } else {
      validInputIngredient = false;
      showDialog(
          context: context,
          builder: (context) {
            return Center(
                child: Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40)),
                    child: Container(
                        height: 250,
                        width: 300,
                        child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                      "<${inputIngredientLabel}> n'est pas un nom d'aliment valide",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontFamily: 'Pacifico',
                                          color: Colors.blueGrey))
                                ])))));
          });
      print("INVALID INGREDIENT");
    }
  }

  //TODO : Revoir les fonctions update/add/validate Ingredients
  //update the local ingredients list
  Future<List<Ingredient>> updateViewIngredients() async {
    ingredients = await controller.getIngredients();
    controller.displayIngredients();
    return ingredients;
    //setState(() {});
  }

  void initIngredientsListView() async {
    List<Ingredient> temp = await updateViewIngredients();
  }

  Future<dynamic> addIngredientsToDbFromOCR(File receiptImage) async {
    //TODO : Call the OCR to transform the receipt image into a json of ingredients
    /*
    //Temporairement : on utilise un json prédéfini
    var jsonResponse = '''
        {
        "ingredients":
          [
            {"name":"lentilles"},
            {"name":"tomates"},
            {"name":"patates"}
          ]
        }
        ''';

     */
    String url = "http://10.0.2.2:8000/api/ocr";
    print("FILE PATH = ${receiptImage.path}");
    print("Last index of '/' = ${receiptImage.path.lastIndexOf('/')}");
    print(
        "FILE NAME = ${receiptImage.path.substring(receiptImage.path.lastIndexOf('/') + 1)}");
    var filePath = receiptImage.path;
    var fileName =
        receiptImage.path.substring(receiptImage.path.lastIndexOf('/') + 1);

    //var jsonResponse = await controller.ocrRequest(url, 'assets/Ticket_de_caisse.JPG', 'Ticket_de_caisse.JPG');
    var jsonResponse = await controller.ocrRequest(url, filePath, fileName);
    var jsonList = jsonDecode(jsonResponse);

    //boucle for avec des appels à addIngredientToDb en faisant un set
    //des variables inputIngredientLabel, inputIngredientQuantity et inputIngredientUnit
    //à partir des valeurs récupérées dans le JSON de la réponse de l'OCR
    for (int i = 0; i < jsonList['ingredients'].length; i++) {
      print("-- : ${jsonList['ingredients'][i]}");
      inputIngredientLabel = jsonList['ingredients'][i]['name'];
      inputIngredientQuantity = '0.0';
      inputIngredientUnit = '-';
      //TODO : gérer l'expiration date ici
      inputIngredientExpirationDate = '2020/06/30';
      //TODO : pour la démo de jeudi matin au pire faire une version loosened de addIngredientToDb à appeler ici
      Future<dynamic> addedIngredient = await addIngredientToDb();
    }
  }

  Future<dynamic> addIngredientToDb() async {
    if (verifyIngredientsExistence()) {
      validInputIngredient = true;
      Future<dynamic> addedIngredient = await controller.addIngredient(
          new Ingredient(
              name: inputIngredientLabel,
              quantity: double.parse(inputIngredientQuantity),
              unit: inputIngredientUnit,
              expirationDate: inputIngredientExpirationDate,
              seasonal: inputIngredientSeasonality));
      return addedIngredient;
    } else {
      validInputIngredient = false;
      showDialog(
          context: context,
          builder: (context) {
            return Center(
                child: Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40)),
                    child: Container(
                        height: 250,
                        width: 300,
                        child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                      "<${inputIngredientLabel}> n'est pas un nom d'aliment valide",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontFamily: 'Pacifico',
                                          color: Colors.blueGrey))
                                ])))));
          });
      print("INVALID INGREDIENT");
    }
  }

  void validateIngredientAddition() async {
    Future<dynamic> addedIngredient = await addIngredientToDb();
    List<Ingredient> temp = await updateViewIngredients();
    setState(() {});
  }

  void validateIngredientAdditionFromOCR(File receiptImage) async {
    Future<dynamic> addedIngredient =
        await addIngredientsToDbFromOCR(receiptImage);
    List<Ingredient> temp = await updateViewIngredients();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    //Load the existing ingredients into the foodstock view
    //updateViewIngredients();
    initIngredientsListView();

    initiateValidIngredientsList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 60,
          //color: Color.fromRGBO(13, 186, 185, 0.8),
          // child: Padding(
          //  padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 8),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Text("Aliment",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Pacifico',
                              color: Colors.blueGrey))
                    ])),
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Text("Quantité",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Pacifico',
                              color: Colors.blueGrey))
                    ])),
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Text("Unité",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Pacifico',
                              color: Colors.blueGrey))
                    ])),
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Text("Modifier",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Pacifico',
                              color: Colors.blueGrey)),
                    ])),
              ]),
          // ),
        ),
        new Expanded(
            child: new ListView.builder(
                itemCount: ingredients.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 1.0),
                    child: Card(
                        child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: IngredientDisplayCard(
                              ingredients[index],
                              () => modifyIngredient(),
                              (temp) => deleteIngredient(temp)),
                        ),
                      ],
                    )),
                  );
                })),
        Row(
          children: <Widget>[
            Expanded(
                child: AddIngredientOCRWidget(
                    (img) => validateIngredientAdditionFromOCR(img)),
                flex: 1),
            Expanded(
              child: AddIngredientManuallyWidget(
                  () => validateIngredientAddition()),
              flex: 3,
            ),
          ],
        )
      ],
    );
  }
}

class AddIngredientOCRWidget extends StatelessWidget {
  File _receiptImage;
  Function addIngredientsFromOCR;
  AddIngredientOCRWidget(this.addIngredientsFromOCR);

  dynamic getImageFromLibrary() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    _receiptImage = image;
    print("image = $_receiptImage");
    return image;
  }

  dynamic getImageFromCamera() async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera);
    _receiptImage = image;
    print("image = $_receiptImage");
    return image;
  }

  void runOCRFromLibrary() async {
    dynamic image = await getImageFromLibrary();
    Future<dynamic> addedIngredientq =
        await addIngredientsFromOCR(_receiptImage);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: FloatingActionButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          //backgroundColor: new Color.fromRGBO(227, 198, 145, 1),
          backgroundColor: Colors.orange[100],
          child: Icon(
            Icons.add_a_photo,
            color: Colors.blueGrey,
          ),
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return Center(
                    child: Dialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        child: Container(
                          height: 300,
                          width: 300,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FlatButton(
                                    //color: Color.fromRGBO(227, 198, 145, 1),
                                    color: new Color(0xFF4bc1d6),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                          "Prendre en photo le ticket de caisse",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontFamily: 'Pacifico',
                                              color: Colors.white)),
                                      //color: Colors.blueGrey)),
                                    ),
                                    onPressed: () {
                                      //TODO: remplacer les 2 méthodes par une seule méthode async, cf le comportement pour le chargement depuis la galerie
                                      getImageFromCamera();
                                      this.addIngredientsFromOCR(_receiptImage);
                                      Navigator.pop(context);
                                    },
                                  ),
                                  SizedBox(height: 30),
                                  FlatButton(
                                    //color: Color.fromRGBO(227, 198, 145, 1),
                                    color: new Color(0xFF4bc1d6),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                          "Charger le ticket de caisse depuis la galerie",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontFamily: 'Pacifico',
                                              color: Colors.white)),
                                      // color: Colors.blueGrey)),
                                    ),
                                    onPressed: () {
                                      runOCRFromLibrary();
                                      Navigator.pop(context);
                                    },
                                  ),
                                ]),
                          ),
                        )),
                  );
                });
          }),
    );
  }
}

class AddIngredientManuallyWidget extends StatelessWidget {
  Function addIngredient;

  AddIngredientManuallyWidget(this.addIngredient);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
      child: FloatingActionButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Text("Ajouter un aliment",
            style: TextStyle(
                color: Colors.white, fontFamily: 'Pacifico', fontSize: 18)),
        // backgroundColor: new Color.fromRGBO(6, 156, 143, 1),
        backgroundColor: new Color(0xFF4bc1d6),
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return Center(
                  child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(40.0),
                        child: Container(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    /*
                                    CustomTextField(
                                        TextFieldType.ingredientLabel,
                                        new InputDecoration(
                                            labelText: 'Aliment',
                                            hintText: 'ex. Abricot'),
                                        TextInputType.text,
                                        [],
                                        true),

                                     */
                                    SimpleAutoCompleteTextField(
                                        key: new GlobalKey(),
                                        controller: TextEditingController(),
                                        suggestions: getValidIngredientsNames(),
                                        textSubmitted: (text) =>
                                            inputIngredientLabel = text,
                                        clearOnSubmit: false,
                                        decoration: new InputDecoration(
                                            labelText: "Nom de l'Aliment",
                                            hintText: 'ex. abricot')),
                                    SizedBox(height: 20),
                                    CustomTextField(
                                        TextFieldType.ingredientQuantity,
                                        new InputDecoration(
                                            labelText: 'Quantité',
                                            hintText: 'ex. 3'),
                                        TextInputType.number,
                                        [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly
                                        ],
                                        true),
                                    SizedBox(height: 20),
                                    CustomTextField(
                                        TextFieldType.ingredientUnit,
                                        new InputDecoration(
                                            labelText: 'Unité',
                                            hintText: 'ex. Kg'),
                                        TextInputType.text,
                                        [],
                                        true),
                                    SizedBox(height: 20),
                                    CustomTextField(
                                        TextFieldType.ingredientExpirationDate,
                                        new InputDecoration(
                                            labelText: 'Date de péremption',
                                            hintText: 'ex. 2020/05/07'),
                                        TextInputType.datetime,
                                        [],
                                        true),
                                    SizedBox(height: 20),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          FlatButton(
                                            child: Text("Annuler"),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                          ),
                                          FlatButton(
                                              child: Text("Ajouter"),
                                              onPressed: () {
                                                addIngredient();
                                                if (validInputIngredient) {
                                                  Navigator.pop(context);
                                                }
                                              }),
                                        ]),
                                  ]),
                            ),
                          ),
                        ),
                      )),
                );
              });
        },
      ),
    );
  }
}

class IngredientDisplayCard extends StatelessWidget {
  Ingredient ingredient;
  Function modifyIngredient;
  Function deleteIngredient;
  IngredientDisplayCard(
      this.ingredient, this.modifyIngredient, this.deleteIngredient);

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Color.fromRGBO(235, 255, 255, 1),
      height: 60,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <
          Widget>[
        Expanded(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text(ingredient.getName())])),
        Expanded(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text(ingredient.getQuantity().toString())])),
        Expanded(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text(ingredient.getUnit())])),
        Expanded(
            child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                    color: Color.fromRGBO(171, 245, 238, 1),
                    padding: const EdgeInsets.only(right: 0.0),
                    child: Icon(Icons.border_color, color: Colors.blueGrey),
                    onPressed: () {
                      //Storing the ingredient's label (its key) before modifying it
                      toBeModifiedIngredientLabel = ingredient.getName();
                      //Showing the ingredient modifier pop-up
                      showDialog(
                          context: context,
                          builder: (context) {
                            return Center(
                              child: Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(40)),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(40.0),
                                    child: Container(
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Padding(
                                          padding: const EdgeInsets.all(12.0),
                                          child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                /*
                                                CustomTextField(
                                                    TextFieldType
                                                        .ingredientLabel,
                                                    new InputDecoration(
                                                        labelText: 'Aliment',
                                                        hintText: ingredient
                                                            .getName()),
                                                    TextInputType.text,
                                                    [],
                                                    true),

                                                 */
                                                SimpleAutoCompleteTextField(
                                                    key: new GlobalKey(),
                                                    controller:
                                                        TextEditingController(),
                                                    suggestions:
                                                        getValidIngredientsNames(),
                                                    textSubmitted: (text) =>
                                                        inputIngredientLabel =
                                                            text,
                                                    clearOnSubmit: false,
                                                    decoration:
                                                        new InputDecoration(
                                                            labelText:
                                                                "Nom de l'Aliment",
                                                            hintText: ingredient
                                                                .getName())),
                                                SizedBox(height: 20),
                                                CustomTextField(
                                                    TextFieldType
                                                        .ingredientQuantity,
                                                    new InputDecoration(
                                                        labelText: 'Quantité',
                                                        hintText: ingredient
                                                            .getQuantity()
                                                            .toString()),
                                                    TextInputType.number,
                                                    [
                                                      WhitelistingTextInputFormatter
                                                          .digitsOnly
                                                    ],
                                                    true),
                                                SizedBox(height: 20),
                                                CustomTextField(
                                                    TextFieldType
                                                        .ingredientUnit,
                                                    new InputDecoration(
                                                        labelText: 'Unité',
                                                        hintText: ingredient
                                                            .getUnit()),
                                                    TextInputType.text,
                                                    [],
                                                    true),
                                                SizedBox(height: 20),
                                                CustomTextField(
                                                    TextFieldType
                                                        .ingredientExpirationDate,
                                                    new InputDecoration(
                                                        labelText:
                                                            'Date de péremption',
                                                        hintText: ingredient
                                                            .getExpirationDate()),
                                                    TextInputType.datetime,
                                                    [],
                                                    true),
                                                SizedBox(height: 20),
                                                Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: <Widget>[
                                                      FlatButton(
                                                        child: Text("Annuler"),
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                      ),
                                                      FlatButton(
                                                          child:
                                                              Text("Modifier"),
                                                          onPressed: () {
                                                            modifyIngredient();
                                                            if (validInputIngredient) {
                                                              Navigator.pop(
                                                                  context);
                                                            }
                                                          }),
                                                    ]),
                                              ]),
                                        ),
                                      ),
                                    ),
                                  )),
                            );
                          });
                    })
              ],
            )),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    color: Colors.orange[100],
                    padding: const EdgeInsets.only(right: 0.0),
                    child: Icon(Icons.delete, color: Colors.blueGrey),
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return Center(
                              child: Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(40)),
                                  child: Container(
                                    height: 250,
                                    width: 300,
                                    child: Padding(
                                      padding: const EdgeInsets.all(12.0),
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                                "Etes-vous sûr de vouloir supprimer l'aliment du garde-manger ?",
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontFamily: 'Pacifico',
                                                    color: Colors.blueGrey)),
                                            SizedBox(height: 20),
                                            Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: <Widget>[
                                                  FlatButton(
                                                    color: new Color.fromRGBO(
                                                        6, 156, 143, 1),
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20)),
                                                    child: Text("Annuler",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white)),
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                  FlatButton(
                                                      color: new Color.fromRGBO(
                                                          6, 156, 143, 1),
                                                      child: Text("Confirmer",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .white)),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20)),
                                                      onPressed: () {
                                                        deleteIngredient(
                                                            ingredient);
                                                        Navigator.pop(context);
                                                      }),
                                                ]),
                                          ]),
                                    ),
                                  )),
                            );
                          });
                    },
                  ),
                ],
              ),
            )
          ],
        )),
      ]),
    );
  }
}

// Custom Text Field : Goal is knowing the values of the added ingredient on "validate" button pressed
class CustomTextField extends StatefulWidget {
  TextFieldType textFieldType;
  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  final textFieldController = TextEditingController();
  Ingredient ingredient;
  InputDecoration decoration;
  TextInputType keyboardType;
  List<TextInputFormatter> inputFormatters;
  bool autofocus;

  CustomTextField(this.textFieldType, this.decoration, this.keyboardType,
      this.inputFormatters, this.autofocus);

  @override
  _MyCustomTextField createState() => _MyCustomTextField();
}

// Define a corresponding State class.
// This class holds data related to the Form.
class _MyCustomTextField extends State<CustomTextField> {
  @override
  void initState() {
    super.initState();

    widget.textFieldController.addListener(() {
      if (widget.textFieldController.text != null) {
        switch (widget.textFieldType) {
          case TextFieldType.ingredientLabel:
            {
              //inputIngredientLabel = widget.textFieldController.text;
            }
            break;

          case TextFieldType.ingredientQuantity:
            {
              inputIngredientQuantity = widget.textFieldController.text;
            }
            break;

          case TextFieldType.ingredientUnit:
            {
              inputIngredientUnit = widget.textFieldController.text;
            }
            break;
          case TextFieldType.ingredientExpirationDate:
            {
              inputIngredientExpirationDate = widget.textFieldController.text;
            }
            break;
        }
      }
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    widget.textFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: widget.decoration,
      keyboardType: widget.keyboardType,
      inputFormatters: widget.inputFormatters,
      controller: widget.textFieldController,
      autofocus: widget.autofocus,
    );
  }
}

class AutocompleteTextField extends StatefulWidget {
  @override
  _AutocompleteTextFieldState createState() => _AutocompleteTextFieldState();
}

class _AutocompleteTextFieldState extends State<AutocompleteTextField> {
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
  SimpleAutoCompleteTextField textField;
  String currentText = "";

  _AutocompleteTextFieldState() {
    textField = SimpleAutoCompleteTextField(
        key: key,
        controller: TextEditingController(text: "Nom de l'ingrédient"),
        suggestions: getValidIngredientsNames(),
        textChanged: (text) => currentText = text,
        textSubmitted: (text) => print("text submitted"));
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
