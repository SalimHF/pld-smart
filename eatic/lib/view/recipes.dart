import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import '../controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'recipeDetails.dart';

var servings = 1;

bool filter_type = false;
var types = [
  "entree",
  "platprincipal",
  "dessert",
  "amusegueule",
  "accompagnement",
  "sauce",
  "boisson",
  "confiserie"
];

var typesBeautified = [
  "Entrée",
  "Plat Principal",
  "Dessert",
  "Amuse-gueule",
  "Accompagnement",
  "Sauce",
  "Boisson",
  "Confiserie"
];

var formatted_type = types.asMap()[1];
var type_selected = typesBeautified.asMap()[1];

bool filter_time = false;
var max_time = 120;

bool filter_difficulty = false;
var difficulty = 2;

bool only_items_owned = false;

class Recipes extends StatefulWidget {
  static const String id = "recipes";

  @override
  _RecipesState createState() => _RecipesState();
}

class _RecipesState extends State<Recipes> {
  Controller controller = Controller();
  bool recettes_loaded = false;
  var jsonResult;

  @override
  void initState() {
    super.initState();
  }

  Future<void> loadJson(var response) async {
    //String data = await rootBundle.loadString('assets/codebeautify.json');
    //jsonResult = json.decode(response);

    jsonResult = response;
    print("response' length = ${jsonResult["recipes"].length}");
    print(response);
    setState(() {
      recettes_loaded = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          (recettes_loaded) ? _buildProgrammCard(jsonResult) : _buildFilter()
        ],
      ),
    );
  }

  Widget _buildFilter() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FilterServings(),
                Text("Sélectionnez des filtres:",
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      decorationThickness: 2.85,
                      fontSize: 14.0,
                      color: Color.fromRGBO(8, 60, 102, 1),
                    )),
                TypeRecipe(),
                MaxTime(),
                Difficulty(),
                OnlyOwned()
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  // backgroundColor: new Color.fromRGBO(6, 156, 143, 1),

                  color: new Color(0xFF4bc1d6),
                  child: Text("Cherchez des recettes",
                      style: TextStyle(fontSize: 16, color: Colors.white)),
                  onPressed: () async {
                    Controller controleur = new Controller();
                    //var servings, var filter_type, var type, var filter_time, var time, var filter_difficulty, var difficulty, var only_owned
                    String requestJson = await controleur.createJsonForRequest(
                        servings,
                        filter_type,
                        formatted_type,
                        filter_time,
                        max_time,
                        filter_difficulty,
                        difficulty,
                        only_items_owned);
                    String url = "http://10.0.2.2:8000/api/search/";
                    // var requestJson = '{"ingredients": [{"name":"tomate"},{"name":"basilic"}]}';
                    //var requestJson = "https://google.com";
                    var jsonResponse =
                        await controleur.sendPostRequest(url, requestJson);
                    loadJson(jsonResponse);
                  }),
            ],
          ),
        ],
      ),
    );
  }
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

Widget _buildProgrammCard(json) {
  return Flexible(
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          padding: const EdgeInsets.all(8),
          itemCount: json["recipes"].length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: new GestureDetector(
                onTap: () {
                  //_launchURL(json["recipies"][index]["link"]);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          RecipeDetails(recette: json["recipes"][index]),
                    ),
                  );
                },
                child: Card(
                  borderOnForeground: true,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.0),
                          topRight: Radius.circular(10.0),
                        ),
                        child: Image.network(
                          json["recipes"][index]["img_link"],
                          height: 150,
                          fit: BoxFit.cover,
                        ),
                      ),
                      ListTile(
                        title: Text(json["recipes"][index]["name"],
                            style: TextStyle(
                                fontSize: 20.0, color: new Color(0xFFF25321))),
                        //TODO : Gérer les cas où la recette contient moins de 3 ingrédients
                        subtitle: Text("Recette à base de " +
                            json["recipes"][index]["ingredients"][0]["name"] +
                            "..."),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Row(children: <Widget>[
                                new Spacer(),
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        (json["recipes"][index]["difficulty"] >=
                                                1)
                                            ? Image.asset(
                                                'assets/details_chef_full.png')
                                            : Image.asset(
                                                'assets/details_chef_empty.png'),
                                        (json["recipes"][index]["difficulty"] >=
                                                2)
                                            ? Image.asset(
                                                'assets/details_chef_full.png')
                                            : Image.asset(
                                                'assets/details_chef_empty.png'),
                                        (json["recipes"][index]["difficulty"] >=
                                                3)
                                            ? Image.asset(
                                                'assets/details_chef_full.png')
                                            : Image.asset(
                                                'assets/details_chef_empty.png'),
                                        (json["recipes"][index]["difficulty"] >=
                                                4)
                                            ? Image.asset(
                                                'assets/details_chef_full.png')
                                            : Image.asset(
                                                'assets/details_chef_empty.png'),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        (json["recipes"][index]["difficulty"] == 1)
                                            ? Text("Très Facile",
                                                style: TextStyle(
                                                    color:
                                                        new Color(0xFFF25321)))
                                            : (json["recipes"][index]["difficulty"] == 2)
                                                ? Text("Facile",
                                                    style: TextStyle(
                                                        color: new Color(
                                                            0xFFF25321)))
                                                : (json["recipes"][index]
                                                            ["difficulty"] ==
                                                        3)
                                                    ? Text("Moyen",
                                                        style: TextStyle(
                                                            color: new Color(
                                                                0xFFF25321)))
                                                    : (json["recipes"][index]["difficulty"] == 4)
                                                        ? Text("Difficile",
                                                            style: TextStyle(
                                                                color: new Color(0xFFF25321)))
                                                        : Text("Not found", style: TextStyle(color: new Color(0xFFF25321))),
                                      ],
                                    )
                                  ],
                                ),
                                new Spacer(),
                                Column(
                                  children: <Widget>[
                                    Row(children: <Widget>[
                                      Icon(
                                        Icons.people,
                                        color: new Color(0xFFF25321),
                                        size: 24.0,
                                      ),
                                    ]),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                            (json["recipes"][index]["servings"])
                                                    .toString() +
                                                " pers.",
                                            style: TextStyle(
                                                color: new Color(0xFFF25321))),
                                      ],
                                    ),
                                  ],
                                ),
                                new Spacer(),
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.timer,
                                          color: new Color(0xFFF25321),
                                          size: 24.0,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                            (json["recipes"][index]["time"]
                                                            ["cooking"] +
                                                        json["recipes"][index]
                                                                ["time"]
                                                            ["preparation"] +
                                                        json["recipes"][index]
                                                            ["time"]["rest"])
                                                    .toString() +
                                                " min",
                                            style: TextStyle(
                                                color: new Color(0xFFF25321))),
                                      ],
                                    )
                                  ],
                                ),
                                new Spacer(),
                                // padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
                                // )
                              ]),
                            ]),
                      ),
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.all(10),
                ),
              ),
            );
          }));
}

class Difficulty extends StatefulWidget {
  @override
  _Difficulty createState() => _Difficulty();
}

class _Difficulty extends State<Difficulty> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      IconButton(
          onPressed: () {
            filter_difficulty = !filter_difficulty;
            setState(() {});
          },
          icon: Icon(
            (filter_difficulty)
                ? Icons.check_box
                : Icons.check_box_outline_blank,
            color: Colors.orange[300],
          )),
      Text("Difficulté: ",
          style: TextStyle(
            fontSize: 14.0,
            color: (filter_difficulty)
                ? Color.fromRGBO(8, 60, 102, 1)
                : Colors.grey,
          )),
      new Spacer(),
      AbsorbPointer(
        absorbing: !filter_difficulty,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    setState(() {
                      difficulty = 1;
                    });
                  },
                  child: (difficulty >= 1)
                      ? ImageIcon(
                          AssetImage('assets/chef_full.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey,
                        )
                      : ImageIcon(AssetImage('assets/chef_empty.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      difficulty = 2;
                    });
                  },
                  child: (difficulty >= 2)
                      ? ImageIcon(
                          AssetImage('assets/chef_full.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey,
                        )
                      : ImageIcon(AssetImage('assets/chef_empty.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      difficulty = 3;
                    });
                  },
                  child: (difficulty >= 3)
                      ? ImageIcon(
                          AssetImage('assets/chef_full.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey,
                        )
                      : ImageIcon(AssetImage('assets/chef_empty.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      difficulty = 4;
                    });
                  },
                  child: (difficulty >= 4)
                      ? ImageIcon(
                          AssetImage('assets/chef_full.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey,
                        )
                      : ImageIcon(AssetImage('assets/chef_empty.png'),
                          color: (filter_difficulty)
                              ? Colors.orange[300]
                              : Colors.grey),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                    (difficulty == 1)
                        ? "Très facile"
                        : (difficulty == 2)
                            ? "Facile"
                            : (difficulty == 3)
                                ? "Moyen"
                                : (difficulty == 4) ? "Difficile" : "",
                    style: TextStyle(
                      fontSize: 14.0,
                      color: (filter_difficulty)
                          ? Color.fromRGBO(8, 60, 102, 1)
                          : Colors.grey,
                    )),
              ],
            )
          ],
        ),
      ),
      new Spacer()
    ]);
  }
}

class OnlyOwned extends StatefulWidget {
  @override
  _OnlyOwned createState() => _OnlyOwned();
}

class _OnlyOwned extends State<OnlyOwned> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      IconButton(
          onPressed: () {
            only_items_owned = !only_items_owned;
            setState(() {});
          },
          icon: Icon(
            (only_items_owned)
                ? Icons.check_box
                : Icons.check_box_outline_blank,
            color: Colors.orange[300],
          )),
      Text("N'utiliser que les aliments que je possède",
          style: TextStyle(
            fontSize: 14.0,
            color: (only_items_owned)
                ? Color.fromRGBO(8, 60, 102, 1)
                : Colors.grey,
          )),
    ]);
  }
}

class MaxTime extends StatefulWidget {
  @override
  _MaxTime createState() => _MaxTime();
}

class _MaxTime extends State<MaxTime> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        IconButton(
            onPressed: () {
              filter_time = !filter_time;
              setState(() {});
            },
            icon: Icon(
              (filter_time) ? Icons.check_box : Icons.check_box_outline_blank,
              color: Colors.orange[300],
            )),
        Text("Temps de cuisine maximal: ",
            style: TextStyle(
              fontSize: 14.0,
              color:
                  (filter_time) ? Color.fromRGBO(8, 60, 102, 1) : Colors.grey,
            )),
        new Spacer(),
        Container(
          width: 40,
          height: 30,
          child: Theme(
            data: new ThemeData(
              primaryColor: Colors.orange[300],
              accentColor: Colors.orange[300],
            ),
            child: TextFormField(
              onChanged: (String value) {
                max_time = int.parse(value);
              },
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly
              ],
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontSize: 14.0,
                color:
                    (filter_time) ? Color.fromRGBO(8, 60, 102, 1) : Colors.grey,
              ),
              cursorColor: Colors.orange[300],
              enabled: filter_time,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  enabledBorder: OutlineInputBorder(
                    // width: 0.0 produces a thin "hairline" border
                    borderSide: BorderSide(color: new Color(0xFFF25321)),
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: new Color(0xFFF25321)),
                  ),
                  hintText: '120'),
            ),
          ),
        ),
        Text(" min",
            style: TextStyle(
              fontSize: 14.0,
              color:
                  (filter_time) ? Color.fromRGBO(8, 60, 102, 1) : Colors.grey,
            )),
        new Spacer()
      ],
    );
  }
}

class TypeRecipe extends StatefulWidget {
  @override
  _TypeRecipe createState() => _TypeRecipe();
}

class _TypeRecipe extends State<TypeRecipe> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        IconButton(
            onPressed: () {
              filter_type = !filter_type;
              setState(() {});
            },
            icon: Icon(
              (filter_type) ? Icons.check_box : Icons.check_box_outline_blank,
              color: Colors.orange[300],
            )),
        Text("Type de plat à cuisiner: ",
            style: TextStyle(
              fontSize: 14.0,
              color:
                  (filter_type) ? Color.fromRGBO(8, 60, 102, 1) : Colors.grey,
            )),
        new Spacer(),
        AbsorbPointer(
          absorbing: !filter_type,
          child: DropdownButton(
            iconEnabledColor: (filter_type) ? Colors.orange[300] : Colors.grey,
            value: type_selected,
            items:
                typesBeautified.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 14.0,
                      color: (filter_type)
                          ? Color.fromRGBO(8, 60, 102, 1)
                          : Colors.grey,
                    )),
              );
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                type_selected = newValue;
                var index = typesBeautified.indexOf(newValue);
                formatted_type = types.asMap()[index];
              });
            },
          ),
        ),
        new Spacer()
      ],
    );
  }
}

class FilterServings extends StatefulWidget {
  @override
  _FilterServings createState() => _FilterServings();
}

class _FilterServings extends State<FilterServings> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("Je souhaite cuisiner pour ",
            style: TextStyle(
              fontSize: 14.0,
              color: Color.fromRGBO(8, 60, 102, 1),
            )),
        IconButton(
            onPressed: () {
              if (servings >= 2) {
                servings--;
              }
              setState(() {});
            },
            icon: Icon(
              Icons.remove_circle_outline,
              color: Colors.orange[300],
            )),
        Text(servings.toString()),
        IconButton(
            onPressed: () {
              servings++;
              setState(() {});
            },
            icon: Icon(
              Icons.add_circle_outline,
              color: Colors.orange[300],
            )),
        Text("personnes",
            style: TextStyle(
              fontSize: 14.0,
              color: Color.fromRGBO(8, 60, 102, 1),
            )),
      ],
    );
  }
}
