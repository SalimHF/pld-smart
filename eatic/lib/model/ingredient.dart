import 'dart:convert';

class Ingredient {
  String name;
  double quantity;
  //TODO : create an enum for the unit property
  String unit;
  String expirationDate;
  bool seasonal;

  Ingredient(
      {String name,
      double quantity,
      String unit,
      String expirationDate,
      bool seasonal}) {
    this.name = name;
    this.quantity = quantity;
    this.unit = unit;
    this.expirationDate = expirationDate;
    this.seasonal = seasonal;
  }

  Ingredient.withName(String name) {
    this.name = name;
  }

  void setName(String name) => this.name = name;

  String getName() => this.name;
  double getQuantity() => this.quantity;
  String getUnit() => this.unit;
  String getExpirationDate() => this.expirationDate;
  bool isSeasonal() => this.seasonal;

  /*
  @override
  toString() => 'IngredientName: $name';
   */

  factory Ingredient.fromJson(Map<String, dynamic> json) => Ingredient(
        name: json["name"],
        quantity: json["quantity"],
        unit: json["unit"],
        expirationDate: json["expirationDate"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "quantity": quantity,
        "unit": unit,
        "expirationDate": expirationDate,
      };

  Ingredient IngredientFromJson(String str) =>
      Ingredient.fromJson(json.decode(str));

  String IngredientToJson(Ingredient data) => json.encode(data.toJson());
}
