import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast/utils/sembast_import_export.dart';
import 'ingredient.dart';
import 'DatabaseSetup.dart';
import 'dart:convert';

class IngredientDao {
  //Name of our table/folder
  static const String folderName = "Ingredients";
  //Init folder
  final _IngredientFolder = intMapStoreFactory.store(folderName);

  //Get db instance
  Future<Database> get _db async => await AppDatabase.instance.database;

  Future insertIngredient(Ingredient ingredient) async {
    await _IngredientFolder.add(await _db, ingredient.toJson());
    print('Ingredient Inserted successfully');
  }

  //Finder = parameter that contains the update on which the records will be updated
  Future updateIngredient(
      String ingredientLabel, Ingredient modifiedIngredient) async {
    //final finder = Finder(filter: Filter.byKey(ingredient.name));
    //final finder = Finder(filter: Filter.equals("name", ingredient.getName()));
    final finder = Finder(filter: Filter.equals("name", ingredientLabel));
    await _IngredientFolder.update(await _db, modifiedIngredient.toJson(),
        finder: finder);
    print('Ingredient Updated successfully');
  }

  Future deleteIngredient(Ingredient ingredient) async {
    final finder = Finder(filter: Filter.equals("name", ingredient.getName()));
    await _IngredientFolder.delete(await _db, finder: finder);
    print('Ingredient Deleted successfully');
    print("INGREDIENT FOLDER $_IngredientFolder.toString())");
  }

  Future<List<Ingredient>> getAllIngredients() async {
    final recordSnapshot = await _IngredientFolder.find(await _db);
    return recordSnapshot.map((snapshot) {
      final ingredient = Ingredient.fromJson(snapshot.value);
      return ingredient;
    }).toList();
  }

  void exportDb() async {
    //Retrieve db
    var content = await exportDatabase(await _db);
    //Save as text
    var saved = jsonEncode(content);
    print(saved);
  }
}
