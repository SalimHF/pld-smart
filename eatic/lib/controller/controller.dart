import 'package:flutter/animation.dart';

import 'package:flutter/services.dart';

import '../model/ingredient.dart';
import '../model/ingredientDao.dart';

import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'dart:io';

class Controller extends ControllerMVC {
  final IngredientDao ingredientDao = IngredientDao();

  Future<dynamic> addIngredient(Ingredient ingredient) =>
      ingredientDao.insertIngredient(ingredient);

  Future<dynamic> updateIngredient(
          String ingredientLabel, Ingredient modifiedIngredient) =>
      ingredientDao.updateIngredient(ingredientLabel, modifiedIngredient);

  Future<dynamic> deleteIngredient(Ingredient ingredient) =>
      ingredientDao.deleteIngredient(ingredient);

  void displayIngredients() {
    Future<List<Ingredient>> ingredients = ingredientDao.getAllIngredients();
    ingredients.then((resultIngredients) {
      print("ingredients length = ${resultIngredients.length}");
      for (int i = 0; i < resultIngredients.length; i++) {
        print("List[$i] = ${resultIngredients[i].getName()}");
      }
    });
  }

  void displayDb() => ingredientDao.exportDb();

  Future<List<Ingredient>> getIngredients() async {
    //List<Ingredient> result = new List<Ingredient>();
    Future<List<Ingredient>> ingredients = ingredientDao.getAllIngredients();
    return ingredients;
  }

  dynamic getIngredientsFromDb() async {
    String url = "http://10.0.2.2:8000/api/ingredient";
    String request = '{"ingredients":[]}';
    var jsonResponse = await sendGetRequest(url, request);
    //print("jsonResponse : $jsonResponse");
    var ingredientList = [];
    for (int i = 0; i < jsonResponse.length; i++) {
      ingredientList.add(new Ingredient(
          name: jsonResponse[i]["name"],
          seasonal: jsonResponse[i]["seasonal"]));
    }
    return ingredientList;
  }

  dynamic sendGetRequest(String url, String request) async {
    print(url);
    print(request);
    //final response = await http.post(url,body:request);
    var response = await http.get(url);
    //print("Server response : ${response.body.toString()}");
    var responseJson = json.decode(utf8.decode(response.bodyBytes));
    return responseJson;
  }

  dynamic sendPostRequest(String url, String request) async {
    print(url);
    print(request);
    final response = await http.post(url, body: request);
    //var response = await http.get(url);
    print(response.body.toString());
    var responseJson = json.decode(utf8.decode(response.bodyBytes));
    return responseJson;
  }

  dynamic ocrRequest(String url, String image_path, String image_name) async {
    //create multipart request for POST or PATCH method
    print('uri = ${Uri.parse(url)}');
    var request = http.MultipartRequest("POST", Uri.parse(url));

    var img = await http.MultipartFile.fromPath(
      'toAnalyze',
      image_path,
      filename: image_name, // use the real name if available, or omit
      contentType: MediaType('image', 'jpeg'),
    );

    //add multipart to request
    request.files.add(img);

    print("Request = ${request.toString()}");
    var response = await request.send();

    //Get the response from the server
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print("Server Response : $responseString");

    return responseString;
  }

  void convertJSONtoIngredientList(ocrResponse) {
    //à voir si cette méthode doit être ici, dans le contrôleur ou dans le modèle
    var ingredientList = [];
    var ingredients = ocrResponse['ingredients'];
    for (int i = 0; i < ingredients.length; i++) {
      //ingredientList.append(ingredients[i])
    }
  }

  Future<String> createJsonForRequest(
      var servings,
      var filter_type,
      var type,
      var filter_time,
      var time,
      var filter_difficulty,
      var difficulty,
      var only_owned) async {
    print("servings " + servings.toString());
    print("filter_type " + filter_type.toString() + " " + type.toString());
    print("filter_time " + filter_time.toString() + " " + time.toString());
    print("filter_diff " +
        filter_difficulty.toString() +
        " " +
        difficulty.toString());
    print("only owned" + only_owned.toString() + " ");

    String jsonRequest = '{"ingredients":[';
    var ingredients = await getIngredients();
    for (int i = 0; i < ingredients.length; i++) {
      if (i == ingredients.length - 1) {
        jsonRequest += '{"name":"' +
            ingredients[i].getName() +
            '"' +
            ', "seasonal": "' +
            ingredients[i].isSeasonal().toString() +
            '","expDate":"' +
            ingredients[i].getExpirationDate() +
            '","quantity":{"unit":"kg","nbr":5}' +
            '}';
      } else {
        jsonRequest += '{"name":"' +
            ingredients[i].getName() +
            '"' +
            ',"seasonal": "' +
            ingredients[i].isSeasonal().toString() +
            '", "expDate":"' +
            ingredients[i].getExpirationDate() +
            '","quantity":{"unit":"kg","nbr":5}' +
            '},';
      }
    }
    bool criteria_added = false;
    jsonRequest += '],"servings":' +
        servings.toString() +
        ', "onlyOwned":' +
        only_owned.toString();
    jsonRequest = jsonRequest + ',"criteria":[';
    if (filter_type) {
      criteria_added = true;
      jsonRequest +=
          '{"class":"qual","name":"recipeType","value":"' + type + '"}';
    }
    if (filter_time) {
      if (criteria_added) {
        jsonRequest += ',{"class":"quant","name":"prepTime","lB":"0","uB":"' +
            time.toString() +
            '"}';
      } else {
        criteria_added = true;
        jsonRequest += '{"class":"quant","name":"prepTime","lB":"0","uB":"' +
            time.toString() +
            '"}';
      }
    }
    if (filter_difficulty) {
      if (criteria_added) {
        jsonRequest += ',{"class":"quant","name":"difficulty","lB":"0","uB":"' +
            difficulty.toString() +
            '"}';
      } else {
        criteria_added = true;
        jsonRequest += '{"class":"quant","name":"difficulty","lB":"0","uB":"' +
            difficulty.toString() +
            '"}';
      }
    }

    jsonRequest +=
        '],"priorities":{"0":[{"name":"expDate"}],"1":[{"name":"seasonal"}],"2":[{"name":"ingredientPresence"}]}';

    jsonRequest = jsonRequest + "}";
    return jsonRequest;
  }
}
