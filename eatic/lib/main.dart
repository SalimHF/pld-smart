import 'package:flutter/material.dart';
import 'package:sembast/sembast_io.dart';
import './view/recipes.dart';
import './view/foodstock.dart';
import './view/recipeDetails.dart';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:sembast/sembast.dart';

void main() {
  runApp(Eatic());
}

class Eatic extends AppMVC {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          accentColor: Colors.orange[100],
          cursorColor: Colors.orange[100],
          focusColor: Colors.orange[100]),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          //backgroundColor: Color.fromRGBO(188, 247, 247, 1), //green blue
          appBar: AppBar(
            //backgroundColor: Color.fromRGBO(13, 186, 185, 1), //green blue
            backgroundColor:
                new Color(0xFF4bc1d6), //Color.fromRGBO(8, 60, 102, 1),
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.kitchen)),
                Tab(icon: Icon(Icons.restaurant_menu)),
              ],
            ),
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  //child: Text('Eatic', style: TextStyle(fontFamily: 'Pacifico', fontSize: 40),
                  child:
                      Image.asset('assets/logo.png', height: 180, width: 180)),
            ),
          ),
          body: TabBarView(
            children: [
              FoodStock(),
              Recipes(),
              //RecipeDetails(),
            ],
          ),
        ),
      ),
    );
  }
}
