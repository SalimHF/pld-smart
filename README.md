**Utilisation**

Eatic consiste en une application mobile qui interagit avec un serveur à travers des requêtes HTTP. 
À ce jour le serveur n’est pas déployé sur internet, il est donc nécessaire de le lancer en local pour pouvoir utiliser les différents services que fournit l’application. 
Il est également nécessaire de faire le build de l’application en local (sur Android Studio ou autre) pour pouvoir utiliser l’application sur un émulateur ou sur son smartphone connecté en USB.

**Packages Python à installer à l'aide de** `pip install <module>`

- OCR : `matplotlib Pillow`
- Serveur : `django==2.2.12 djangorestframework django-filters==2.2.0`

**Lancer l’application**

Deux méthodes :
- Installer l’APK directement sur son smartphone ou sur un émulateur et lancer l’application
- Cloner le projet GitLab et build l’application depuis Android Studio (ou autre IDE) puis l’exécuter sur son smartphone connecté en USB ou sur un émulateur

Comme indiqué dans la section précédente, seule la deuxième méthode permet d’accéder au serveur et ainsi à toutes les fonctionnalités de l’application. 

**Lancer le serveur**

Pour configurer le serveur correctement :
- Lancer `python manage.py migrate` dans le dossier serveatic

Pour peupler la base de données : 
- Depuis la racine : `python serveatic/api/webcrawler/initBD.py`

**Pour lancer le serveur** 

`python manage.py runserver 127.0.0.1:8000`
